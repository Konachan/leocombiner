import argparse
import os
import pathlib

argparser = argparse.ArgumentParser()
argparser.add_argument("--dir", "-d", help="Directory with .ldr files", required=True)
argparser.add_argument("--out", "-o", help="Output .mpd file")

args = argparser.parse_args()

if args.out is None:
    args.out = "out.mpd"

outfile = open(args.out, "w")

for root, _, files in os.walk(args.dir):
    for file in files:
        if pathlib.Path(file).suffix == ".ldr":
            filepath = pathlib.Path(root)/file
            fileContent = open(filepath, "r").read()

            outfile.write("0 FILE " + str(filepath) + "\n")
            outfile.write(fileContent)
            outfile.write("0 NOFILE\n")

outfile.write("0 FILE complete.ldr\n0\n0 Name: complete.ldr\n0 Author: \n0 NOFILE")

outfile.flush()
outfile.close()

